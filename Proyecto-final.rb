require "colorize"

def hacer_tablero() # crea el tablero y lo mantiene en standby
    datos = ["USA","ARG","BRA","CHL","DEU","ITA","MEX","RUS","ESP","VEN","USA","ARG","BRA","CHL","DEU","ITA","MEX","RUS","ESP","VEN"] # datos a introducir en el tablero
    matriz = []
    for e in 0..5-1 do
        matriz[e] = []
        for x in 0..4-1 do
            random = rand(datos.length-1) # aleatorio del arreglo para que cada juego sea diferente (pares de cartas)
            matriz[e][x] = [datos[random],false] # la matriz contiene un arreglo con un string y un booleano en cada posicion y se le asgina false para que no aparezcan volteadas
            datos.delete_at(random) # se retira un valor del arreglo  que especifica la variable random y lo introduce en cada una de las posiciones de la matriz
        end
    end
    return matriz
end

def fijar_tablero(matriz,card_1,card_2,aciertos,movimientos) # Pone el tablero del juego en pantalla
    puts
    puts "Aciertos:     " + aciertos.to_s
    puts "Movimientos:  " + movimientos.to_s 
    puts
    for e in 0..5-1 do
        for x in 0..4-1 do
            if matriz[e][x][1] == true then
                print "|" + matriz[e][x][0].to_s + "|" # imprime el tablero con las cartas volteadas
            elsif (card_1 != nil && card_1[0].to_i == e && card_1[1].to_i == x) then # muestra card_1
                print "|" + matriz[e][x][0].to_s + "|"
            elsif (card_2 != nil && card_2[0].to_i == e && card_2[1].to_i == x) then # muestra card_2
                print "|" + matriz[e][x][0].to_s + "|"
            else
                print "|".colorize(:yellow) +"C".colorize(:blue) + e.to_s.colorize(:blue) + x.to_s.colorize(:blue) + "|".colorize(:yellow) # imprime cada una de las coordenadas para mejor interaccion con el usuario
            end
        end
        puts
    end
end

def jugada_1() 
    puts "Seleccione una Carta"
    card_1 = gets.chomp
    return card_1
end

def jugada_2()
    puts "Seleccione otra Carta"
    card_2 = gets.chomp
    return card_2
end

def comprobar_jugadas(matriz,card_1,card_2,aciertos) # comprueba la jugada para saber si son iguales o no las 2 cartas levantadas, aparte lleva el conteo de aciertos
    if matriz[card_1[0].to_i][card_1[1].to_i][0] == matriz[card_2[0].to_i][card_2[1].to_i][0] then # comprueba si card_1 y card_2 son iguales
        matriz[card_1[0].to_i][card_1[1].to_i][1] = true  
        matriz[card_2[0].to_i][card_2[1].to_i][1] = true  # se asgina un true a card_1 y card_1 para dejarlas volteadas durante todo el juego
        aciertos = aciertos + 1 # variable para determinar el conteo de aciertos, como la jugada es corecta suma 1 cada vez
    
        puts "      Correcto".colorize(:green)
    else
        puts "     Incorrecto".colorize(:red)
        puts "Vuelva a Intentarlo"
    end
    return [matriz,aciertos]
end

def game_over(matriz) # evalua si las cartas se encuentran en false para determinar si se acaba el juego
    condicion = true
    for e in 0..5-1 do
        for x in 0..4-1 do
            if matriz[e][x][1] == false then 
                condicion = false # se le asigna false para llevar el conteo de pares de cartas volteadas
            end
        end
    end
    return condicion
end

def puntuacion_final(aciertos,movimientos) # se especifica que el juego ha terminado y la puntuacion final de la partida
    puts
    resultado = (aciertos * 2) - movimientos # variable para determinar la puntuacion final
    puts "Juego Terminado..."
    puts "Su Puntuacion Final es:   " + resultado.to_s
end

def nuevo_record(record,aciertos,movimientos) # determina si hay un nuevo record y en tal caso de que lo hubiese guarda dicha puntuacion final para evaluarlos con otros record durante la ejecucion del programa
    resultado = (aciertos * 2) - movimientos
    if record.empty? then                      # si es la primera partida no deberia haber ningun record por lo que este seria nuestro primer record 
        puts "-------------------------------"
        puts "Nuevo Record... FELICITACIONES"
        puts "-------------------------------"
        record.push(resultado)               # variable para guardar la puntuacion final en un arreglo
    elsif record[0].to_i < resultado.to_i    # compara la puntuacion actual con records anteriores y si es superior especifica que hay un nuevo record
        puts "-------------------------------"
        puts "Nuevo Record... FELICITACIONES"
        puts "-------------------------------"
        record[0] = resultado     # sobreescribimos el valor de record para guardar el nuevo record
    else
        puts "-------------------------------"
        puts "Sigue Intentando Superarme Pequeño :P" 
        puts "-------------------------------"
    end
    puts
    return record
end


def playing()
    record = []
    system("clear")
    loop do 
        matriz = hacer_tablero() # crea el tablero
        movimientos = 0 # variable para llevar el conteo de movimientos, incialmente comienza en 0
        aciertos = 0 # variable para llevar el conteo de momovimientos, incialmente comienza en 0
        card_1 = nil 
        card_2 = nil
        loop do
            puts
            fijar_tablero(matriz,card_1,card_2,aciertos,movimientos)
            puts
            card_1 = jugada_1()
            system("clear")
            puts
            fijar_tablero(matriz,card_1,card_2,aciertos,movimientos)
            puts
            card_2 = jugada_2()
            system("clear")
            movimientos = movimientos + 1
            resultados = comprobar_jugadas(matriz,card_1,card_2,aciertos)
            matriz = resultados[0]
            aciertos = resultados[1]
            fijar_tablero(matriz,card_1,card_2,aciertos,movimientos)
            puts
            puts "Presione Enter para Continuar"
            gets.chomp
            system("clear")       
            card_1 = nil
            card_2 = nil
            break if (game_over(matriz) == true)
        end
        record = nuevo_record(record,aciertos,movimientos)
        fijar_tablero(matriz,card_1,card_2,aciertos,movimientos)
        puntuacion_final(aciertos,movimientos)
        puts "Desea Jugar Nuevamente Y/N"
        opcion = gets.chomp.to_s
        system("clear")
        break if (opcion == "N" || opcion == "n")
    end
end

playing()



